# Discovery
La app cuenta con 2 botones.
- Botón "on/off" sirve para encender y apagar el bluetooth del dispositivo
- Botón "Discovery" sirve para poner el dispositivo en discover y ser visible para otros dispositivos

Hay un Textview que en un principio no contiene texto, pero dependiendo de si mi dispositivo fue encontrado en la asistencia del día en el que la app se ejecuta (es decir, se corre la app el mismo día que llaman la asistencia) muestra el mensaje de que fue encontrado o no.

Se utiliza la librería de anko para hacer uso de la función async y poder acceder a la URL donde se encuentran los datos. Esto ocurre porque el hilo que se encarga de ir a la URL se ejecuta sobre el hilo que corre el main de la app.

**Funcionamiento**

La app se ejecuta el día que se llame lista. Si el dispositivo ya fue encontrado y registrado en la asistencia se retornará de que se encontró en la asistencia de ese día, de lo contrario se notificará que el dispositivo no fue encontrado.

**Nota:**
Hay una línea de código que sirve para probar la asistencia del día 5 de agosto. Se debe comentar la línea que toma la fecha dinámica y poner la que está comentada. El mensaje de todos modos retornará la fecha actual pues la app está pensada para retornar los resultados en base a la fecha actual y sólo es una prueba para ver el mensaje exitoso.

